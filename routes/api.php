<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UsuarioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['guest:api']], function() {
    Route::get('usuarios', [UsuarioController::class, 'getAll']);
    Route::post('usuario', [UsuarioController::class, 'getByEmail']);
    Route::post('usuario/add', [UsuarioController::class, 'store']);
    Route::post('usuario/update/{usuario}', [UsuarioController::class, 'update']);
    Route::delete('usuario/{usuario}', [UsuarioController::class, 'delete']);
});

