<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use App\Http\Requests\UsuarioRequest;
use App\Http\Requests\UpdateUsuarioRequest;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{

    public function getAll(){
        $usuarios = Usuario::get();
        return response()->json($usuarios);
    }

    public function getByEmail(Request $request){
        $usuario = Usuario::where('email',$request['email'])->first();
        return response()->json($usuario);
    }

    public function store(UsuarioRequest $request)
    {
        

        $usuario=new Usuario;
        $usuario->fill($request->all());
        $usuario->save();

        return response()->json($usuario);
    }

    public function update(UpdateUsuarioRequest $request, Usuario $usuario){
        $usuario = $usuario->update($request->all());

        return response()->json($usuario);
        
    }


    public function delete(Usuario $usuario){
        $usuario = $usuario->delete();
        return response()->json($usuario);
    }

}
