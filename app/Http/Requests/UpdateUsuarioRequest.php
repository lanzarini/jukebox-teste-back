<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\FormRequestApi;

class UpdateUsuarioRequest extends FormRequestApi
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string|max:191',
            'sobrenome' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:usuarios,email,'.$this->id,
            'telefone' => 'required|string|max:191',
            'cpf' => 'required_if:pessoa_juridica,0|string|max:11|nullable|unique:usuarios,cpf,'.$this->id,
            'cnpj' => 'required_if:pessoa_juridica,1|string|max:14|nullable|unique:usuarios,cnpj,'.$this->id,
            'pessoa_juridica' => 'required|boolean'
        ];
    }
}
