<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\FormRequestApi;

class UsuarioRequest extends FormRequestApi
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string|max:191',
            'sobrenome' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:usuarios,email',
            'telefone' => 'required|string|max:191',
            'cpf' => 'required_if:pessoa_juridica,0|string|max:11|unique:usuarios,cpf|nullable',
            'cnpj' => 'required_if:pessoa_juridica,1|string|max:14|unique:usuarios,cnpj|nullable',
            'pessoa_juridica' => 'required|boolean'
        ];
    }
}
